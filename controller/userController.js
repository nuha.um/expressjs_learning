const { response } = require("express")

let users = [
    {id:1, nama:"nuha"},
    {id:2, nama:"ulin"},
    {id:3, nama:"muhammad"},
]

module.exports = {
    index: function(req, res) {
        res.render('pages/users/index', {users})
    },
    create: function(req, res) {
        res.render('pages/users/create')
    }
}