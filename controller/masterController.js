let masters = [
    {id:1, nama:"nuha"},
    {id:2, nama:"ulin"},
    {id:3, nama:"muhammad"},
]

module.exports = {
    index: function (req, res) {
        // res.send("master get from router group file")
        if (masters.length>0) {
            res.json({
                status: true,
                data: masters,
                method: req.method,
                url: req.url
            })
        } else {
            res.json({
                status: false,
                message: 'Master Null'
            })
        }
    },
    store: function (req, res) {
        // res.send("master post from router group file")
        masters.push(req.body)
        res.send({
            status: true,
            message: 'Data Berhasil di Simpan',
            data: masters,
            method: req.method,
            url: req.url
        })
    },
    update: function (req, res) {
        const id = req.params.id
        masters.filter(master=>{
            if (master.id == id) {
                master.id = id
                master.nama = req.body.nama
            }
            return master
        })
        res.send({
            status: true,
            message: 'Data Berhasil di Perbarui',
            data: masters,
            method: req.method,
            url: req.url
        })
    },
    delete: function (req, res) {
        let id = req.params.masterId
        masters = masters.filter(master=>master.id != id)
        res.send({
            status: true,
            message: 'Data Berhasil di Hapus',
            data: masters,
            method: req.method,
            url: req.url
        })
    },
}