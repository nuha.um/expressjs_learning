const express = require('express')
const masterRoute = require('./routes/routes')
const app = express()

app.use(express.json()) // parsing json
app.use(express.urlencoded({extended:true})) //parsing form
app.use('/assets', express.static('public'))
app.set('view engine', 'ejs')
app.get('/', function (request, response) {
    // response.send('Hello Word!')
    const data = {
        nama:'muhammad',
        umur: 15
    }
    response.render('index', {data:data})
})

app.get('/about', function (req, res) {
    res.send('About')
})
// method get
// app.get('/users', function (req, res) {
//     res.send('User Get')
// })
// // method post
// app.post('/users', function (req, res) {
//     res.send('User Post')
// })
// // method put
// app.put('/users/:id', function (req, res) {
//     res.send(req.params)
// })
// // method delete
// app.delete('/users/:userId', function (req, res) {
//     res.status(200)
//     res.send(req.params.userId)
// })
// // res redirect
// app.get('/redirect', function (req, res) {
//     res.redirect('/about')    
// })
// // res redirect
// app.get('/res-status', function (req, res) {
//     res.status(200).end()   
// })
// // route group
// app.route('/pengguna')
//     .get(function (res, req) {
//         res.send('pengguna route group get')
//     })
//     .post(function (res, req) {
//         res.send('pengguna route group post')
//     })

app.use(masterRoute)

app.listen(3000, function () {
    console.log('server is okay');
})