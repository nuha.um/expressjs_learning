const express = require('express')
const router = express.Router()
const masterController = require('../controller/masterController')
const userController = require('../controller/userController')
// master
router.route('/master')
    .get(masterController.index)
    .post(masterController.store)
router.put('/master/:id', masterController.update)
router.delete('/master/:masterId', masterController.delete)
// user
router.route('/user')
    .get(userController.index)
router.route('/user/create')
    .get(userController.create)
//     .post(userController.store)
// router.put('/user/:id', userController.update)
// router.delete('/user/:userId', userController.delete)

module.exports = router